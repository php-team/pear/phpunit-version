phpunit-version (6.0.0-1) experimental; urgency=medium

  * Upload new major to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Fri, 07 Feb 2025 18:50:40 +0100

phpunit-version (5.0.2-2) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 11

 -- David Prévot <taffit@debian.org>  Fri, 10 Jan 2025 09:34:54 +0100

phpunit-version (5.0.2-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

  [ Cristian Rodríguez ]
  * Avoid spawning a shell for git

 -- David Prévot <taffit@debian.org>  Thu, 10 Oct 2024 10:27:01 +0100

phpunit-version (5.0.1-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Use PHPStan instead of Psalm
  * Prepare release

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Sun, 14 Jul 2024 11:49:13 +0200

phpunit-version (5.0.0-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Start development of next major version
  * Declare class readonly
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Sat, 03 Feb 2024 12:44:42 +0100

phpunit-version (4.0.1-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

 -- David Prévot <taffit@debian.org>  Fri, 10 Feb 2023 07:35:11 +0100

phpunit-version (4.0.0-1) experimental; urgency=medium

  * Upload new major version to experimental

  [ Sebastian Bergmann ]
  * Drop support for PHP 7.3
  * Drop support for PHP 7.4
  * Reformat so that tools recognize the license
  * Bump copyright year
  * Add Security Policy
  * Prepare release

  [ David Prévot ]
  * Check upstream signature
  * Update copyright (years and license)
  * Ship upstream security notice
  * Set upstream metadata fields: Security-Contact.
  * Update standards version to 4.6.2, no changes needed.
  * Override dh_auto_clean

 -- David Prévot <taffit@debian.org>  Sun, 05 Feb 2023 14:49:44 +0100

phpunit-version (3.0.2-3) unstable; urgency=medium

  * Fix d/watch to match updated GitHub URL
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Install /u/s/pkg-php-tools/{autoloaders,overrides} files
  * Mark package as Multi-Arch: foreign
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Sun, 03 Jul 2022 14:07:55 +0200

phpunit-version (3.0.2-2) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 9
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Sun, 20 Dec 2020 14:32:26 -0400

phpunit-version (3.0.2-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Wed, 30 Sep 2020 13:59:42 -0400

phpunit-version (3.0.1-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Support PHP 8 for https://github.com/sebastianbergmann/phpunit/issues/4325

  [ David Prévot ]
  * Use debhelper-compat 13

 -- David Prévot <taffit@debian.org>  Mon, 29 Jun 2020 11:10:29 -1000

phpunit-version (3.0.0-1) experimental; urgency=medium

  * Upload version compatible with PHPUnit 9 to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Drop support for PHP 7.1 and PHP 7.2
  * Prepare release

  [ David Prévot ]
  * debian/copyright: Update copyright (years)
  * debian/upstream/metadata:
    + Set fields: Bug-Database, Bug-Submit, Repository, Repository-Browse
    + Remove obsolete fields: Contact, Name
  * debian/control:
    + Drop versioned dependency satisfied in (old)stable
    + Update standards version to 4.5.0

 -- David Prévot <taffit@debian.org>  Fri, 07 Feb 2020 18:33:14 -1000

phpunit-version (2.0.1-2) unstable; urgency=medium

  * Use versioned copyright format URI.
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Contact, Name.
  * Move repository to salsa.d.o
  * Drop get-orig-source target
  * Update Standards-Version to 4.4.0

 -- David Prévot <taffit@debian.org>  Wed, 21 Aug 2019 19:54:45 -1000

phpunit-version (2.0.1-1) unstable; urgency=medium

  [ Sebastian Bergmann ]
  * Use best practice for adding this library as a dependency using Composer

  [ David Prévot ]
  * Update Standards-Version to 4.0.0

 -- David Prévot <taffit@debian.org>  Fri, 04 Aug 2017 16:45:20 -0400

phpunit-version (2.0.0-2) unstable; urgency=medium

  * Upload to unstable now that all reverse-dependencies are ready
  * Update Standards-Version to 3.9.8

 -- David Prévot <taffit@debian.org>  Tue, 03 May 2016 15:33:40 -0400

phpunit-version (2.0.0-1) experimental; urgency=medium

  * Upload to experimental since most reverse-dependencies currently rely on
    phpunit-version (<< 2~~)

  [ Sebastian Bergmann ]
  * Bump

  [ David Prévot ]
  * Update Standards-Version to 3.9.7

 -- David Prévot <taffit@debian.org>  Fri, 05 Feb 2016 15:03:36 -0400

phpunit-version (1.0.6-1) unstable; urgency=medium

  [ Sebastian Bergmann ]
  * Cleanup

  [ David Prévot ]
  * Adapt packaging to upstream clean up

 -- David Prévot <taffit@debian.org>  Wed, 24 Jun 2015 10:44:31 -0400

phpunit-version (1.0.5-2) unstable; urgency=medium

  * Upload to unstable since Jessie has been released

 -- David Prévot <taffit@debian.org>  Wed, 03 Jun 2015 15:40:50 -0400

phpunit-version (1.0.5-1) experimental; urgency=medium

  [ Henrique Moody ]
  * Update license and copyright in all files

  [ David Prévot ]
  * Update copyright years
  * Drop #777380 workaround

 -- David Prévot <taffit@debian.org>  Fri, 03 Apr 2015 12:31:07 -0400

phpunit-version (1.0.4-1) experimental; urgency=medium

  [ David Prévot ]
  * Fix source and homepage URLs
  * Adapt packaging to Composer source
  * Bump standards version to 3.9.6
  * Upload to experimental to respect the freeze

  [ Jeff Welch ]
  * Remove PEAR.

  [ Jonas Stendahl ]
  * Always use last git segment.

 -- David Prévot <taffit@debian.org>  Sun, 08 Feb 2015 16:50:44 -0400

phpunit-version (1.0.3-1) unstable; urgency=low

  * Initial Release (Closes: #745846)

 -- David Prévot <taffit@debian.org>  Fri, 25 Apr 2014 15:23:45 -0400
